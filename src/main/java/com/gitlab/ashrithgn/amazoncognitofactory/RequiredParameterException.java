package com.gitlab.ashrithgn.amazoncognitofactory;

public class RequiredParameterException extends Throwable {
    public RequiredParameterException(String message) {
        super(message);
    }
}
