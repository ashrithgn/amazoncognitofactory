package com.gitlab.ashrithgn.amazoncognitofactory;


import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClient;
import com.amazonaws.services.cognitoidp.model.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CognitoInstance {

    private  String poolId;
        private  String clientId;
        protected AWSCognitoIdentityProvider cognitoClient;
        private String tempPassword = "have a nice Day _";
        private String region;

    public CognitoInstance(String poolId, String clientId,String awsAccessKey,String awsSecretKey) {
        this.poolId = poolId;
        this.clientId =clientId;
        this.region = poolId.split("_")[0];
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(awsAccessKey, awsSecretKey);
        cognitoClient = AWSCognitoIdentityProviderClient.builder().withRegion(Regions.fromName(region)).withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .build();
    }


    public void registerUser(String username, Map<String,String> userAttributeKeyValuePairs,boolean shouldConfirmUser,String password) throws Throwable {

        if(shouldConfirmUser == true){
            if(password == null){
                throw new RequiredParameterException("password cannot be null");
            }
            if(password == ""){
                throw new RequiredParameterException("password cannot be empty");
            }
        }

        AdminCreateUserRequest cognitoRequest = new AdminCreateUserRequest();
        cognitoRequest.setUsername(username);
        cognitoRequest.setUserPoolId(poolId);

        if(shouldConfirmUser == true){
            cognitoRequest.setTemporaryPassword(tempPassword);
        }

        List<AttributeType> userAttributes = new ArrayList<AttributeType>();
        if(userAttributeKeyValuePairs.containsKey("email")){
            userAttributeKeyValuePairs.put("email_verified","true");
        }

        for (String key:userAttributeKeyValuePairs.keySet()) {
            AttributeType attributeType = new AttributeType();
            attributeType.setName(key);
            attributeType.setValue(userAttributeKeyValuePairs.get(key));
            userAttributes.add(attributeType);
        }

        cognitoRequest.setUserAttributes(userAttributes);
        cognitoRequest.setForceAliasCreation(false);
        try{
            AdminCreateUserResult acr =  cognitoClient.adminCreateUser(cognitoRequest);
            if(shouldConfirmUser == true){
                AuthenticationResultType res =  confirmUser(tempPassword,password,username);
            }
        } catch (Throwable e){
            throw e;
        }

    }

    public AdminInitiateAuthResult userLogin(String username,String password) throws Throwable {
        try{
            Map<String,String> authParams = new HashMap<String,String>();
            authParams.put("USERNAME", username);
            authParams.put("PASSWORD", password);

            AdminInitiateAuthRequest authRequest = new AdminInitiateAuthRequest()
                    .withAuthFlow(AuthFlowType.ADMIN_NO_SRP_AUTH)
                    .withAuthParameters(authParams)
                    .withClientId(clientId)
                    .withUserPoolId(poolId)
                    ;
            AdminInitiateAuthResult authResponse = cognitoClient.adminInitiateAuth(authRequest);
            return authResponse;
        }catch (Throwable e){
            throw e;
        }

    }


    public AuthenticationResultType confirmUser(String tempPassword,String password,String username){
        Map<String,String> initialParams = new HashMap<String,String>();
        initialParams.put("USERNAME", username);
        initialParams.put("PASSWORD", tempPassword);
        AdminInitiateAuthRequest initialRequest = new AdminInitiateAuthRequest()
                .withAuthParameters(initialParams)
                .withAuthFlow(AuthFlowType.ADMIN_NO_SRP_AUTH)
                .withClientId(clientId)
                .withUserPoolId(poolId);

        AdminInitiateAuthResult initialResponse = cognitoClient.adminInitiateAuth(initialRequest);
        Map<String,String> challengeResponses = new HashMap<String,String>();
        challengeResponses.put("USERNAME", username);
        challengeResponses.put("PASSWORD", tempPassword);
        challengeResponses.put("NEW_PASSWORD", password);
        AdminRespondToAuthChallengeRequest finalRequest = new AdminRespondToAuthChallengeRequest()
                .withChallengeName(ChallengeNameType.NEW_PASSWORD_REQUIRED)
                .withChallengeResponses(challengeResponses)
                .withClientId(clientId)
                .withUserPoolId(poolId)
                .withSession(initialResponse.getSession());
         return cognitoClient.adminRespondToAuthChallenge(finalRequest).getAuthenticationResult();

    }


    public GetUserResult authorize(String accessToken) throws Throwable {
        try{
            GetUserRequest authRequest = new GetUserRequest().withAccessToken(accessToken);
            GetUserResult authResponse = cognitoClient.getUser(authRequest);
            return authResponse;
        }catch (Throwable t){
            throw t;
        }

    }



    public String getTempPassword() {
        return tempPassword;
    }

    public void setTempPassword(String tempPassword) {
        this.tempPassword = tempPassword;
    }
}
